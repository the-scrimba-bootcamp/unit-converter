## Unit Converter Project Requirements
1. Follows the design spec at https://www.figma.com/file/cqtGul0V8RFXY4vTcIv1Kc/Unit-Conversion?node-id=0%3A1
2. Generates all conversions when the user clicks on the "Convert" button
3. Rounds the numbers down to 3 decimal places