const conversionValueInput = document.querySelector(".conversion-value");
const convertUnitBtn = document.querySelector(".convert-button");

const lengthDomEl = document.querySelector(".length > p");
const volumeDomEl = document.querySelector(".volume > p");
const massDomEl = document.querySelector(".mass > p");

const conversionOptions = ["length", "volume", "mass"];

addEventListener("load", function(){
    let randomNumber = Math.floor(Math.random() * 999);

    conversionValueInput.value = randomNumber;
    for (let i = 0; i < conversionOptions.length; i++) {
        convertUnits(conversionOptions[i], conversionValueInput.value);
    }

} )

convertUnitBtn.addEventListener("click", function(){
    for (let i = 0; i < conversionOptions.length; i++) {
        convertUnits(conversionOptions[i], Number(conversionValueInput.value));
    }

});

// Takes what's provided in the text input field and converts it to  versions of 
// Length (meter/feet), Volume (Liters/Gallons), and Mass (kilograms/pounds)

/**
 * Add two numbers.
 * @param {string} unitType
 * @param {number} unitAmount
 * @returns {Array}
 */
function convertUnits(unitType, unitAmount){

    if (unitType === "length") {
        let convertedResult =  convertLength(unitAmount);
        let lengthSentence = `${unitAmount} meters = ${convertedResult[0]} feet | ${unitAmount} feet = ${convertedResult[1]} meters`;
        return renderToDom(lengthDomEl, lengthSentence)
        // return convertedResult;
    }

    // if another type:
    else if (unitType === "volume") {
        let convertedResult =  convertVolume(unitAmount);
        let volumeSentence = `${unitAmount} liters = ${convertedResult[0]} gallons | ${unitAmount} gallons = ${convertedResult[1]} liters`;
        return renderToDom(volumeDomEl, volumeSentence);
    }


     // if another type:
     else if (unitType === "mass") {
        let convertedResult = convertMass(unitAmount);
        let massSentence = `${unitAmount} kilos = ${convertedResult[0]} pounds | ${unitAmount} pounds = ${convertedResult[1]} kilos`;
        return renderToDom(massDomEl, massSentence);
        // 20 kilos = 44.092 pounds | 20 pounds = 9.072 kilos
    }
};

function convertLength(unitAmount){
    // Convert from meters to feet:
    const oneMeterInFeet = parseFloat(3.281);
    const metersToFeet = (unitAmount * oneMeterInFeet).toFixed(3);

    // Convert from feet to meters:
    const feetToMeters = (unitAmount / oneMeterInFeet).toFixed(3);
    return [metersToFeet, feetToMeters];
};


function convertVolume(unitAmount){
    // Convert from liters to gallons:
    const oneLiterInGallons = 0.264;
    const litersToGallons = (unitAmount * oneLiterInGallons).toFixed(3);

    // Convert from gallons to liters:
    const gallonsToMeters = (unitAmount / oneLiterInGallons).toFixed(3);
    return [litersToGallons, gallonsToMeters];

};


function convertMass(unitAmount){
    // Convert from liters to gallons:
    const oneKilogramInPounds = 2.204;
    const kilogramsToPounds = (unitAmount * oneKilogramInPounds).toFixed(3);

    // Convert from gallons to liters:
    const poundsToKilograms = (unitAmount / oneKilogramInPounds).toFixed(3);
    return [kilogramsToPounds, poundsToKilograms];
};

// DOM RENDER element's innerHTML
function renderToDom(dataSource, targetDom) {
    return dataSource.innerHTML = targetDom;
}